package com.ing.wheatherinfo;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;

import com.ing.wheatherinfo.controller.WheatherAnalyzerController;
import com.ing.wheatherinfo.dao.WheatherInfoDAOImpl;
import com.ing.wheatherinfo.domain.WheatherInfo;
import com.ing.wheatherinfo.exception.InvalidWheatherInfoException;


@RunWith(MockitoJUnitRunner.class)
public class TestWeatherAnalyzerDAO {

	WheatherInfo weatherInfo;
	
	@InjectMocks
	WheatherInfoDAOImpl wheatherInfoDAO ;
	
	@Mock
	JdbcTemplate jdbcTemplate;
	
	@InjectMocks
	WheatherAnalyzerController wheatherAnalyzerController;
	
	@Before
	public void getMockWeatherInfo(){
		weatherInfo = new WheatherInfo();
		weatherInfo.setId(999);
		weatherInfo.setHumidity(87);
		weatherInfo.setTemperature(34.2);
		weatherInfo.setTimeStamp(new Timestamp(System.currentTimeMillis()));
		weatherInfo.setPlace("brl");
	}
	
	@Test
	public void testWheatherInfoDao() {
		String insertQuery = "INSERT INTO WEATHERINFO VALUES (?,?,?,?,?)";
		String maxIdQuery = "SELECT MAX(ID) FROM WEATHERINFO";
		Mockito.when(jdbcTemplate.update(insertQuery , new Object[] { 999 , weatherInfo.getPlace() , weatherInfo.getTemperature() ,
				weatherInfo.getHumidity() , weatherInfo.getTimeStamp() })).thenReturn(1);
		Mockito.when(jdbcTemplate.queryForObject(maxIdQuery, Integer.class)).thenReturn(999);
		wheatherInfoDAO.createWheatherInfo(weatherInfo);
		
	}
	
	
	@Test(expected = InvalidWheatherInfoException.class)
	public void testWheatherInfoDaoInValidWeatherInfoData(){
		weatherInfo.setHumidity(890);
		weatherInfo.setTemperature(89);
		weatherInfo.setPlace("");
		weatherInfo.setTimeStamp(new Timestamp(System.currentTimeMillis()));
		
		wheatherInfoDAO.createWheatherInfo(weatherInfo);
		
	}
}
