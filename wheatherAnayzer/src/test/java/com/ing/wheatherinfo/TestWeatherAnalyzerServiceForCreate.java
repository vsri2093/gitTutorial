package com.ing.wheatherinfo;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ing.wheatherinfo.dao.WheatherInfoDAOImpl;
import com.ing.wheatherinfo.domain.WheatherInfo;
import com.ing.wheatherinfo.service.WheatherInfoServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TestWeatherAnalyzerServiceForCreate {

	WheatherInfo wheatherInfo = null;
	
	@InjectMocks
	WheatherInfoServiceImpl wheatherInfoService;
	
	@Mock
	WheatherInfoDAOImpl wheatherInfoDAO;
	
	@Before
	public void populateWheatherInfo(){
		wheatherInfo = new WheatherInfo();
		wheatherInfo.setHumidity(90);
		wheatherInfo.setTemperature(19.9);
		wheatherInfo.setPlace("Delhi");
		wheatherInfo.setTimeStamp(new Timestamp(System.currentTimeMillis()));
	}
	
	@Test
	public void testWheatherInfoServiceForCreate(){
		
		wheatherInfoService.createWheatherInfo(wheatherInfo);
	}
}
