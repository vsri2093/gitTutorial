package com.ing.wheatherinfo;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ing.wheatherinfo.controller.WheatherAnalyzerController;
import com.ing.wheatherinfo.domain.WheatherInfo;
import com.ing.wheatherinfo.service.WheatherInfoService;

@RunWith(MockitoJUnitRunner.class)
public class TestWheatherInfoControllerForCreate {

	WheatherInfo wheatherInfo;
	
	@InjectMocks
	WheatherAnalyzerController wheatherAnalyzerController;
	
	@Mock
	WheatherInfoService wheatherInfoService;
	
	@Before
	public void prepareWheatherInfo(){
		wheatherInfo = new WheatherInfo();
		wheatherInfo.setHumidity(99);
		wheatherInfo.setPlace("Delhi");
		wheatherInfo.setTemperature(11);
		wheatherInfo.setTimeStamp(new Timestamp(System.currentTimeMillis()));;
	}
	
	@Test
	public void test() {
		
		wheatherAnalyzerController.createWheatherInfo(wheatherInfo);
	}

}
