package com.ing.wheatherinfo.dao;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ing.wheatherinfo.domain.WheatherInfo;

@Repository("wheatherInfoDao")
public class WheatherInfoDAOImpl implements WheatherInfoDAO {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public WheatherInfo createWheatherInfo(WheatherInfo wheatherInfo) {
		
		

		String insertQuery = "INSERT INTO WEATHERINFO VALUES (?,?,?,?)";
		int maxID = 100;//getMaxIdWheatherInfo();
		System.out.println("In DAO create method ----------------");
		jdbcTemplate.update(insertQuery , new Object[] {wheatherInfo.getTemperature(), wheatherInfo.getTimeStamp(), wheatherInfo.getPlace() , wheatherInfo.getHumidity()  });
		
		return populateWheatherInfo(wheatherInfo, maxID);
	}

	private int getMaxIdWheatherInfo() {

		String idMacxQuery = "SELECT MAX(ID) FROM WEATHERINFO";
		System.out.println("In get MaxId method ------------"+jdbcTemplate.toString());
		int maxID = jdbcTemplate.queryForObject(idMacxQuery, Integer.class);
		return maxID+1;
	}
	
	private WheatherInfo populateWheatherInfo(WheatherInfo wheatherInfo, int wheatherInfoId)
	{
		WheatherInfo newWheatherInfo=new WheatherInfo();
		System.out.println("In populate method ------------------");
		BeanUtils.copyProperties(wheatherInfo, newWheatherInfo);
		newWheatherInfo.setId(wheatherInfoId);
		System.out.println(newWheatherInfo.toString());
		return newWheatherInfo;
	}
}
