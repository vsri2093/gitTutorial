package com.ing.wheatherinfo.dao;

import com.ing.wheatherinfo.domain.WheatherInfo;

// weatherinfo dao

public interface WheatherInfoDAO {

	public WheatherInfo createWheatherInfo(WheatherInfo wheatherInfo);
	
}
