package com.ing.wheatherinfo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ing.wheatherinfo.domain.WheatherInfo;
import com.ing.wheatherinfo.service.WheatherInfoService;

@Controller
@RequestMapping(value = "/wheatherinfo")
public class WheatherAnalyzerController {
	
	@Autowired
	WheatherInfoService wheatherInfoService;
	
	@RequestMapping(value = "/create",method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE )
	public ResponseEntity<WheatherInfo> createWheatherInfo(@RequestBody WheatherInfo wheatherInfo){
		System.out.println("In create method ------------");
		wheatherInfoService.createWheatherInfo(wheatherInfo);
		return new ResponseEntity<>(wheatherInfo, HttpStatus.CREATED);
	}
}
