package com.ing.wheatherinfo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ing.wheatherinfo.dao.WheatherInfoDAO;
import com.ing.wheatherinfo.domain.WheatherInfo;

@Service
public class WheatherInfoServiceImpl implements WheatherInfoService{
	
	@Autowired 
	WheatherInfoDAO wheatherInfoDAO;

	public WheatherInfo createWheatherInfo(WheatherInfo wheatherInfo){
		System.out.println("In Service create method----------");
		wheatherInfo = wheatherInfoDAO.createWheatherInfo(wheatherInfo);
		return wheatherInfo;
	}
}
