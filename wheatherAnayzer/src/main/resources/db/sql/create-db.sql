CREATE TABLE users (
  id         INTEGER PRIMARY KEY,
  firstname VARCHAR(100),
  lastname VARCHAR(100),
  city VARCHAR(50),
  email  VARCHAR(50)
);

CREATE TABLE weatherinfo (
  id INTEGER IDENTITY PRIMARY KEY,
  temperature DECIMAL(5, 2),
  timeStamp TIMESTAMP,
  place VARCHAR(50),
  humidity  DECIMAL(5, 2)
);