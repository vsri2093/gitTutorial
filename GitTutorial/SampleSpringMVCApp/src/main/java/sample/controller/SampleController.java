package sample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sample.repository.Sample;

@Controller
public class SampleController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public @ResponseBody Sample getSample() {
		Sample sample = new Sample();
		sample.setId(10);
		sample.setName("Murthy");
		return sample;
	}
}
